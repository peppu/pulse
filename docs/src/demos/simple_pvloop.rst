..  #!/usr/bin/env python
  # -*- coding: utf-8 -*-
  
.. _simple_pvloop:

.. py:currentmodule:: pulse

Simple square-like pV-loop
==========================

This example shows how to produce a pV-loop with no circulation model.

Given a `p_atrium` (preload) and `p_aorta` (afterload), this simulates
a rectangular pV-loop with vertices:

- (`p_atrium`, `V_EDP`) => end-diastolic point
- (`p_aorta`,  `V_EDP`) => aorta valve opening
- (`p_aorta`,  `V_ESP`) => end-sistolic point
- (`p_atrium`, `V_ESP`) => mitral valve opening

Ejection and passive filling are done with constant pressure.

The mechanical state of the ventricle is defined by the

- displacement `u`
- hydrostatic pressure `p`
- activation level `gamma`
- inner pressure `p_inner`
- inner volume `V_inner`

::

  # System imports
  import os.path
  import numpy as np
  
  # Local imports
  from dolfin import *
  from pulse.lvproblem import LVProblem
  from pulse.itertarget import itertarget
  from pulse.datacollector import DataCollector
  from pulse.geometry import LVProlateEllipsoid
  from pulse.material import HolzapfelOgden
  
We only want standard output on rank 0. We therefore set the log level to
ERROR on all other ranks

::

  comm = mpi_comm_world()
  if mpi_comm_world().rank == 0:
      set_log_level(INFO)
  else:
      set_log_level(ERROR)
  
Read geometry from file. If the file is not present we regenerate it.

::

  meshname = "./simple_pvloop.h5"
  geoparam = LVProlateEllipsoid.default_parameters()
  geoparam['mesh_generation']['ndiv'] = 2
  geoparam['mesh_generation']['order'] = 2
  geoparam['axisymmetric'] = True
  geoparam['microstructure']['analytic_expression'] = False
  regen = not os.path.isfile(meshname)
  geo = LVProlateEllipsoid(meshname, "", comm, regen, geoparam)
  
Construct the material and attach fiber structures from geometry.

::

  matparam = HolzapfelOgden.default_parameters()
  
  mat = HolzapfelOgden(matparam)
  
Construct a LV problem, which will be used to generate the simple pV loop.

::

  params = LVProblem.default_parameters()
  params["bc_type"] = 'fix_base'
  problem = LVProblem(geo, mat, params=params)
  
Data collector for postprocessing results

::

  collector = DataCollector("./results", problem)
  
Numerical constants setting the boundaries for the pV loop

::

  p_atrium = 0.1
  p_aortic = 1.0
  gamma_max = 0.2
  max_adapt_iter = 12
  
Save displacements for later processing

::

  ofile = File("simple_inflation.xdmf")
  ofile << (Function(problem.get_displacement(), name = "u"), 0.0)
  info("VOLUME = {}".format(problem.get_Vendo()))
  
Passive inflation up to p_atrium

::

  itertarget(problem, target_end=p_atrium, target_parameter="pressure",
             control_step=0.02, control_parameter="pressure",
             control_mode="pressure", data_collector=collector,
             max_adapt_iter=max_adapt_iter)
  
  ofile << (Function(problem.get_displacement(), name="u"), 1.0)
  info("VOLUME = {}".format(problem.get_Vendo()))
  
Isovolumic contraction. Adjusting gamma to reach aortic pressure

::

  itertarget(problem, target_end=p_aortic, target_parameter="pressure",
             control_step=0.01, control_parameter="gamma", control_mode="volume",
             data_collector=collector, max_adapt_iter=max_adapt_iter)
  
  ofile << (Function(problem.get_displacement(), name = "u"), 2.0)
  info("VOLUME = {}".format(problem.get_Vendo()))
  
Isotonic contraction. Adjusting gamma to reach gamma max

::

  itertarget(problem, target_end=gamma_max, target_parameter="gamma",
             control_step=0.01, control_parameter="gamma", control_mode="pressure",
             data_collector=collector, max_adapt_iter=max_adapt_iter)
  
  ofile << (Function(problem.get_displacement(), name = "u"), 3.0)
  info("VOLUME = {}".format(problem.get_Vendo()))
  
Isovolumic relaxation. Adjusting gamma negatively to reach atrial pressure.

::

  itertarget(problem, target_end=p_atrium, target_parameter="pressure",
             control_step=-0.01, control_parameter="gamma",
             control_mode="volume", data_collector=collector,
             max_adapt_iter=max_adapt_iter)
  
  ofile << (Function(problem.get_displacement(), name = "u"), 4.0)
  info("VOLUME = {}".format(problem.get_Vendo()))
  
Isotonic relaxation. Adjusting gamma negatively to reach zero active force.

::

  itertarget(problem, target_end=0.0, target_parameter="gamma", control_step=-0.01,
             control_parameter="gamma", control_mode="pressure", data_collector=collector,
             max_adapt_iter=max_adapt_iter)
  
  ofile << (Function(problem.get_displacement(), name = "u"), 5.0)
  info("VOLUME = {}".format(problem.get_Vendo()))
