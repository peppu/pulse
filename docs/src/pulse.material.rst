Passive material models
=======================

HolzapfelOgden
--------------

.. automodule:: pulse.material.holzapfelogden
    :members:
    :undoc-members:
    :show-inheritance:

Guccione
--------

.. automodule:: pulse.material.guccione
    :members:
    :undoc-members:
    :show-inheritance:

Active material models
======================

.. automodule:: pulse.material.activation
    :members:
    :undoc-members:
    :show-inheritance:

