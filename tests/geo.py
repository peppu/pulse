from dolfin import *
from pulse.geometry import LVProlateEllipsoid
from pulse.geometry import LVSimpleEllipsoid
from pulse.geometry import LVGeometry

import os.path

set_log_level(PROGRESS)
parameters['form_compiler']['representation'] = 'uflacs'
parameters['form_compiler']['quadrature_degree'] = 4
parameters['allow_extrapolation'] = True

def test_geotype(cls) :
    name = cls.__name__
    meshname = "{}.h5".format(name)
    coordname = "{}_coords.pvd".format(name)
    lbasename = "{}_localbase.pvd".format(name)

    regen = True#not os.path.isfile(meshname)
    meshname = ''

    param = cls.default_parameters()
    param['mesh_generation']['ndiv'] = 2
    param['mesh_generation']['order'] = 1
    param['axisymmetric'] = True
    param['microstructure']['function_space'] = 'Lagrange_1'
    param['microstructure']['interpolate'] = True
    geo = cls(meshname,"",mpi_comm_world(),regen,param)

    print "Volume({}) = {}".format(cls.__name__, geo.inner_volume())

    Vv = VectorFunctionSpace(geo.domain, "P", 1, 3)
    Vt = TensorFunctionSpace(geo.domain, "P", 1, shape = (3, 3))

    coords = Expression(cppcode=geo._compile_cart2coords_code(),degree=1)
    File(coordname) << interpolate(coords, Vv)

    localbase = Expression(cppcode = geo._compile_localbase_code())
    localbase.cart2coords = coords
    File(lbasename) << interpolate(localbase, Vt)

    oname = '{}.xdmf'.format(name)
    out = XDMFFile(oname)
    out.parameters['rewrite_function_mesh'] = False
    out.parameters['time_series'] = False
    for f in ['f0','s0','n0']:
        out.write(getattr(geo,f))
    del out

if __name__ == "__main__" :
    test_geotype(LVProlateEllipsoid)
    #test_geotype(LVSimpleEllipsoid)
