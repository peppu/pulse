from dolfin import *
from pulse.bivproblem import BiVProblem
from pulse.itertarget import itertarget
from pulse.itertarget import itertarget2
from pulse.datacollector import DataCollector
from fenicshotools.geometry import *
from pulse.geometry import BiVGeometry
from pulse.material import HolzapfelOgden
from itertools import izip
import h5py
import matplotlib.pyplot as plt


import os.path
import numpy as np

parameters['form_compiler']['representation'] = 'uflacs'



def biv_pv_loop(problem, p_ED_lv, p_ED_rv, pressure, volume, time):
    
    #kpa -> N/cm2
    scale = 10
    ofile = File("biv_test.xdmf")
    ofile << (Function(problem.get_displacement(), name = "u"), 0.0)
    info("VOLUME LV= {}".format(problem.get_Vendo(vent = "LV")))
    info("VOLUME RV= {}".format(problem.get_Vendo(vent = "RV")))
    info("AREA = {}".format(problem.get_epiarea()))
    
    psteplv = p_ED_lv / (5*scale)
    psteprv = p_ED_rv / (5*scale)
    counter = 1.0

    # passive inflation
    itertarget2(problem,
                target_end = [p_ED_lv/scale, p_ED_rv/scale], data_collector=collector,
               target_parameter = "pressure", control_step = [psteplv, psteprv],
               control_parameter = "pressure", control_mode = "pressure")

   
    ofile << (Function(problem.get_displacement(), name = "u"), counter)
    info("VOLUME LV= {}".format(problem.get_Vendo(vent = "LV")))
    info("VOLUME RV= {}".format(problem.get_Vendo(vent = "RV")))
    info("AREA = {}".format(problem.get_epiarea()))

    # target EDV
    itertarget(problem,
           target_end = volume[0], data_collector=collector,
           target_parameter = "volume", control_step = 0.1,
           control_parameter = "a_fibers", control_mode = "pressure")

    counter += 1.0
    ofile << (Function(problem.get_displacement(), name = "u"), counter)
    info("VOLUME RV= {}".format(problem.get_Vendo(vent = "LV")))
    info("AREA = {}".format(problem.get_epiarea()))


    for p,v in izip(pressure[1:], volume[1:]):

    	itertarget(problem,
               target_end = p/scale, data_collector=collector,
               target_parameter = "pressure", control_step = 0.1,
               control_parameter = "gamma", control_mode = "volume")

	counter += 1.0
    	ofile << (Function(problem.get_displacement(), name = "u"), counter)
    	info("VOLUME LV= {}".format(problem.get_Vendo(vent = "LV")))
	info("VOLUME RV= {}".format(problem.get_Vendo(vent = "RV")))
    	info("AREA = {}".format(problem.get_epiarea()))


	itertarget(problem,
           target_end = v, data_collector=collector,
           target_parameter = "volume", control_step = 0.1,
           control_parameter = "gamma", control_mode = "pressure")


    	counter += 1.0
    	ofile << (Function(problem.get_displacement(), name = "u"), counter)
    	info("VOLUME LV= {}".format(problem.get_Vendo(vent = "LV")))
	info("VOLUME RV= {}".format(problem.get_Vendo(vent = "RV")))
    	info("AREA = {}".format(problem.get_epiarea()))




if __name__ == "__main__" :

    comm = mpi_comm_world()

    if MPI.rank(comm) == 0:
        set_log_level(INFO)
    else:
        set_log_level(ERROR)

    # geometry
    h5name = "biv_mesh.h5"

    pressure =  [3.,14., 3.]
    volume = [250.,190., 250.]
    
    geo = BiVGeometry.from_file(h5name, h5group='DS', comm=comm)
 
    # material
    matparam = HolzapfelOgden.default_parameters()
    matparam['f0'] = geo.f0
    
    mat = HolzapfelOgden(**matparam)

    # problem
    param = BiVProblem.default_parameters()
    param["bc_type"] = 'fix_base_ver'

    problem = BiVProblem(geo, mat, **param)

    collector = DataCollector("./results_biv", problem)

    # iterations
    biv_pv_loop(problem, 3.,2., pressure, volume, time)
    
