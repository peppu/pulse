"""Compute eigenstrains at ED point and ES point for LV ellipsoid."""

# System imports
import os.path
import numpy as np

# Local imports
from fenics import *
from pulse.lvproblem import LVProblem
from pulse.itertarget import itertarget
from pulse.datacollector import DataCollector
from pulse.geometry import LVProlateEllipsoid
from pulse.material import HolzapfelOgden

parameters['allow_extrapolation'] = True

# This code produce an Expression that evaluates the eigenvalues and
# eigenvectors for a Tensor field, point-wise. The behaviour is controlled
# by `mode`, that is we return the vector of eigenvalues for mode = 0, while
# for mode > 0 we return the eigenvector corresponding to `mode` eigenvalue.
code = \
"""\
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

namespace dolfin
{

class EigenValuesExpr: public Expression
{
public:
    typedef Eigen::Vector3d vec_type;
    typedef Eigen::Matrix3d mat_type;

    std::shared_ptr<dolfin::GenericFunction> Afun;
    int mode;

    EigenValuesExpr(): Expression(3),mode(0)
    {}

    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x,
              const ufc::cell& cell) const
    {
        assert(this->Afun);

        dolfin::Array<double> Adata(9);
        Afun->eval(Adata,x,cell);
        mat_type A = Eigen::Map<mat_type>(Adata.data());

        Eigen::EigenSolver<mat_type> es(A);

        switch (mode)
        {
            case 1:
            case 2:
            case 3:
                Eigen::Map<vec_type>(values.data()) = es.eigenvectors().col(mode-1).real();
                break;
            default:
            case 0:
                Eigen::Map<vec_type>(values.data()) = es.eigenvalues().real();
                break;
        };
    }

};

};
"""

# Eigen doesn't sort eigenvalues in any particular order, but we are interested
# in the maximum one. We return it and the corresponding eigenvector.
codemax = \
"""\
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

namespace dolfin
{

class MaxEigenValueExpr: public Expression
{
public:
    typedef Eigen::Vector3d vec_type;
    typedef Eigen::Matrix3d mat_type;

    std::shared_ptr<dolfin::GenericFunction> Ffun;
    std::shared_ptr<dolfin::GenericFunction> f0fun;

    MaxEigenValueExpr(): Expression(5)
    {}

    void eval(dolfin::Array<double>& values,
              const dolfin::Array<double>& x,
              const ufc::cell& cell) const
    {
        assert(this->Afun);

        dolfin::Array<double> Fdata(9);
        Ffun->eval(Fdata,x,cell);
        mat_type F = Eigen::Map<mat_type>(Fdata.data());
        mat_type C = F.transpose() * F;

        dolfin::Array<double> f0data(3);
        f0fun->eval(f0data,x,cell);
        vec_type f0 = Eigen::Map<vec_type>(f0data.data());

        Eigen::SelfAdjointEigenSolver<mat_type> es(C);

        vec_type::Index idx;
        auto emax = es.eigenvalues().maxCoeff(&idx);
        auto vmax = es.eigenvectors().col(idx);

        values[0] = emax;
        values[1] = vmax[0];
        values[2] = vmax[1];
        values[3] = vmax[2];
        values[4] = std::abs(vmax.dot(f0));
    }

};

};
"""


def project_tensor(A,Aproj,geo):
    V = Aproj.function_space()
    if geo.is_axisymmetric():
        X = SpatialCoordinate(geo.domain)
        Z,R = X[0],X[1]
        Jgeo = 2.0 * DOLFIN_PI * R
    else:
        Jgeo = 1.0

    aform = inner(TrialFunction(V),TestFunction(V))*Jgeo*dx
    Lform = inner(A,TestFunction(V))*Jgeo*dx

    solve(aform == Lform,Aproj)

    return Aproj


# We only want standard output on rank 0. We therefore set the log level to
# ERROR on all other ranks
comm = mpi_comm_world()
if mpi_comm_world().rank == 0:
    set_log_level(INFO)
else:
    set_log_level(ERROR)

# Various parameters for the simulation
geop = LVProlateEllipsoid.default_parameters()
geop['axisymmetric'] = True
geop['mesh_generation']['ndiv'] = 2
geop['mesh_generation']['order'] = 2
geop['microstructure']['interpolate'] = True
geop['microstructure']['function_space'] = 'Quadrature_6'
matp = HolzapfelOgden.default_parameters()
ppar = LVProblem.default_parameters()
ppar["bc_type"] = 'fix_base_ver'
ppar['form_compiler_parameters']['quadrature_degree'] = 6
ppar['recompute_jacobian']['always'] = True

geo = LVProlateEllipsoid(comm=comm,parameters=geop)
mat = HolzapfelOgden(matp)
problem = LVProblem(geo,mat,params=ppar)

# we need fibers interpolated on Lagrange space
geo._parameters['microstructure']['function_space'] = 'Lagrange_2'
f0,_,_ = geo._generate_microstructure(geo.domain,geo.mesh,geo.phi)
geo._parameters['microstructure']['function_space'] = 'Quadrature_6'

u = problem.get_displacement()
F = geo.deformation_gradient(u)
C = F.T*F

# current fibers
ff = Function(f0.function_space())
project_tensor(F*f0/sqrt(dot(F*f0,F*f0)),ff,geo)

# we need to project C to be able to evaluate the expression
fe  = FiniteElement("Lagrange",geo.domain.ufl_cell(),2)
vfe = VectorElement(fe,dim=3)
tfe = TensorElement(fe,shape=(3,3))
T = FunctionSpace(geo.domain,tfe)

Fproj = Function(T,name="defgrad")
project_tensor(F,Fproj,geo)
eigfe = VectorElement(fe,dim=5)
exprmax = Expression(cppcode=codemax,element=eigfe)
exprmax.Ffun = Fproj
exprmax.f0fun = ff
#exprmax.f0fun = f0

V4 = FunctionSpace(geo.domain,eigfe)
eigmax = Function(V4,name="eigenvalues")
eigmax.interpolate(exprmax)

# solve the problem to ED
p_atrium = 0.1
p_aortic = 1.0
gamma_max = 0.2

info("VOLUME = {}".format(problem.get_Vendo()))

itertarget(problem,target_end=p_atrium,target_parameter="pressure",
           control_step=0.01,control_parameter="pressure",
           control_mode="pressure")

info("VOLUME = {}".format(problem.get_Vendo()))

V  = FunctionSpace(geo.domain,fe)
Vv = FunctionSpace(geo.domain,vfe)
e1 = Function(V,name="maxeig")
v1 = Function(Vv,name="eigvec")
dd = Function(V,name="f0dotv1")

project_tensor(F*f0/sqrt(dot(F*f0,F*f0)),ff,geo)
project_tensor(F,Fproj,geo)
eigmax.interpolate(exprmax)

assign(e1,eigmax.sub(0))
assign(v1,[eigmax.sub(1),eigmax.sub(2),eigmax.sub(3)])
assign(dd,eigmax.sub(4))

meshref = refine(geo.domain)
fe1 = FiniteElement("P",geo.domain.ufl_cell(),1)
Vref = FunctionSpace(meshref,fe1)
ddref = Function(Vref,name="f0_dot_vmax")
ddref.interpolate(dd)

ofile = XDMFFile(comm,"myeig.xdmf")
ofile.parameters['rewrite_function_mesh'] = False
ofile.write(ddref,0.0)
#ofile.write(e1)
#ofile.write(v1)

#print dd.vector().array()
#print e1.vector().array()
#print v1.vector().array()

itertarget(problem,target_end=p_aortic,target_parameter="pressure",
           control_step=0.01,control_parameter="pressure",
           control_mode="pressure")
itertarget(problem,target_end=gamma_max,target_parameter="gamma",
           control_step=0.01,control_parameter="gamma",control_mode="pressure")

info("VOLUME = {}".format(problem.get_Vendo()))

project_tensor(F*f0/sqrt(dot(F*f0,F*f0)),ff,geo)
project_tensor(F,Fproj,geo)
eigmax.interpolate(exprmax)

assign(e1,eigmax.sub(0))
assign(v1,[eigmax.sub(1),eigmax.sub(2),eigmax.sub(3)])
assign(dd,eigmax.sub(4))

ddref.interpolate(dd)
ofile.write(ddref,1.0)

