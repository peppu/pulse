clear; clc;

% mass matrix (singular)
M = zeros(4); M(4,4) = 1;

% pre-filling
V_init = 1.1;
nlfun = @(w) simple_heart(w(1), w(2), w(3), V_init, 0.0);
w = fsolve(nlfun, [1.0, 1.0, 0.0], optimoptions('fsolve', 'Display', 'off'));

y0 = [ w, V_init ];

if 1
    opts = odeset('Mass', M, 'MassSingular', 'yes');
    [T, Y] = ode15s(@(t, y) lumped_heart(t, y), [0.0, 30.0], y0, opts);
    
else
    T = linspace(0.0, 30.0, 1000);
    dt = T(2) - T(1);
    Y = zeros(length(T), length(y0));
    Y(1,:) = y0;

    for i = 2:length(T)
        t = T(i);
        yp = Y(i-1,:).';
        fun = @(y) M*(y - yp) - dt*lumped_heart(t, y);
        %Y(:, i) = newton();
        Y(i,:) = fsolve(fun, yp, optimoptions('fsolve', 'Display', 'off'));
    end
end

subplot(2, 2, 1);
plot(T, Y(:,1:2), 'LineWidth', 2.0);
title('Deformation');
legend('lambda_f', 'lambda_s');
grid on;
subplot(2, 2, 2);
plot(Y(:, 4), Y(:, 3), 'r-*', 'LineWidth', 2.0);
title('pV-loop');
grid on;
subplot(2, 2, 3);
plot(T, Y(:, 3), 'LineWidth', 2.0);
title('Pressure LV');
grid on;
subplot(2, 2, 4);
plot(T, Y(:, 4), 'LineWidth', 2.0);
title('Volume LV');
grid on;
