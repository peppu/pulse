function res = ho_actstress(lambda_f, lambda_s, f_act)

    % material parameters (from Wang et al. (2013))
    % 1 N/cm2 = 10 kPa
    a_iso =  0.02362;
    b_iso = 10.810;
    a_f   =  2.0037;
    b_f   = 14.154;
    a_s   =  0.37245;
    b_s   =  5.1645;

    % microstructure
    f0 = [ 1; 0; 0 ];
    s0 = [ 0; 1; 0 ];
    n0 = [ 0; 0; 1 ];

    % deformation
    lambda_n = 1/(lambda_f*lambda_s);
    
    F = lambda_f * (f0 * f0.') + ...
        lambda_s * (s0 * s0.') + ...
        lambda_n * (n0 * n0.');
    C = F.'*F;
    B = F*F.';
    
    % invariants
    I1  = trace(C);
    I4f = dot(C*f0, f0);
    I4s = dot(C*s0, s0);
    
    % strain-energy derivatives
    dW1  = a_iso/2 * exp(b_iso*(I1 - 3));
    dW4f = a_f * subplus(I4f - 1) * exp(b_f*(I4f - 1)^2);
    dW4s = a_s * subplus(I4s - 1) * exp(b_s*(I4s - 1)^2);
    
    % residual
    res = zeros(2, 1);
        
    % passive cauchy stress
    Tpas = 2.0 * dW1 * B ...
         + 2.0 * dW4f * ((B*f0) * f0.') ...
         + 2.0 * dW4s * ((B*s0) * s0.');
    
    % active stress
    Tact = f_act * ((B*f0) * f0.');
    
    % reaction constraint for incompressibility
    % assumption: n0 direction stress-free
    Tinc = - dot(Tpas*n0, n0)*eye(3);
    
    % stress balance
    T = Tpas + Tact + Tinc;
    
    % since n0 direction is stress-free, balance is given by
    res(1) = dot(T*f0, f0);
    res(2) = dot(T*s0, s0);
    
end
