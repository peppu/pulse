function dy = lumped_heart_dynamics(t, y)

    % lambda_f' = v_f
    %      v_f' = - f_1(lambda_f, lambda_s, p_fluid)
    % lambda_s' = v_s
    %      v_s' = - f_2(lambda_f, lambda_s, p_fluid)
    %        0  = f_3(lambda_s, V_fluid)
    
    % state
    lambda_f = y(1);
    v_f      = y(2);
    lambda_s = y(3);
    v_s      = y(4);
    p_fluid  = y(5);
    V_fluid  = y(6);

    % active force (from electrophysiology)
    f_act = 0.5 * subplus(-sin(0.5*pi*t));
    
    dy = zeros(5, 1);
    
    % mechanical model
    tmp = simple_heart(lambda_f, lambda_s, p_fluid, V_fluid, f_act);
    
    dy(1) = v_f;
    dy(2) = - tmp(1);
    dy(3) = v_s;
    dy(4) = - tmp(2);
    dy(5) = tmp(3);
    
    % circulation model
    p_atrium = 0.12;
    p_aorta  = 0.4;
    k_mitral = 10.0;
    k_aorta  = 0.8;
 
    dy(6) = k_mitral * subplus(p_atrium - p_fluid) ...
          - k_aorta  * subplus(p_fluid - p_aorta);
 
end
