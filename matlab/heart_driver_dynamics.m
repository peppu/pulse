clear; clc;

% mass matrix (singular)
M = eye(6); M(5,5) = 0;

% pre-filling
p_atrium = 0.2;
nlfun = @(w) simple_heart(w(1), w(2), p_atrium, w(3), 0.0);
w = fsolve(nlfun, [1.0, 1.0, 1.0], optimoptions('fsolve', 'Display', 'off'));

% initial state
y0 = [ w(1), 0.0, w(2), 0.0, p_atrium, w(3) ];

T = linspace(0.0, 30.0, 1000);
dt = T(2) - T(1);
Y = zeros(length(T), length(y0));
Y(1,:) = y0;

for i = 2:length(T)
    t = T(i);
    yp = Y(i-1,:).';
    fun = @(y) M*(y - yp) - dt*lumped_heart_dynamics(t, y);
    Y(i,:) = fsolve(fun, yp, optimoptions('fsolve', 'Display', 'off'));
end

subplot(2, 2, 1);
plot(T, Y(:,[1,3]), 'LineWidth', 2.0);
hold on;
plot(T, Y(:,[2,4]), '--', 'LineWidth', 2.0);
title('Deformation');
legend('lambda_f', 'lambda_s', 'v_f', 'v_s');
grid on;
subplot(2, 2, 2);
plot(Y(:, 6), Y(:, 5), 'r-*', 'LineWidth', 2.0);
title('pV-loop');
grid on;
subplot(2, 2, 3);
plot(T, Y(:, 5), 'LineWidth', 2.0);
title('Pressure LV');
grid on;
subplot(2, 2, 4);
plot(T, Y(:, 6), 'LineWidth', 2.0);
title('Volume LV');
grid on;
