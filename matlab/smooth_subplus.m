function y = smooth_subplus(x, a)
% mollifier of the subplus function
    
    y = 0.5*(1/a*log(cosh(a*x)) + x) + 0.346574/a;

end