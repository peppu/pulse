Pulse
=====

FEniCS solver for mechanics and electrophysiology of the heart.

Usage without installing the package
------------------------------------
```bash
python -m tests.simple_pvloop
```

Installation of Gmsh
--------------------------
Download source code from [Gmsh source](geuz.org/gmsh/src/gmsh-2.8.5-source.tgz)

```
#!bash

mkdir make
cd make
ccmake ..
```

In the gui set the following flags to "on"
ENABLE_MPI
ENABLE_BUILD_LIB
ENABLE_BUILD_SHARED

now install gmesh

```
#!bash

make
sudo make install
```

Finally make sure that the environment variable $LD_LIBRARY_PATH includes the directory /usr/local/lib.