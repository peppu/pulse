"""This module implements an Solver class that takes a nonlinear
problem and performs newton iterations to solve the problem"""
# Copyright (C) 2014-2015 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *

__all__ = ["LVSolver"]

class LVSolver(object):
    def __init__(self, use_snes=True, max_iterations=50):
        self.reset(use_snes, max_iterations=max_iterations)

    def solve(self, problem):
        problem._first_iteration = True
        problem._prev_residual = 1.
        problem._recompute_jacobian = True
        result = self.solver.solve(problem, problem._state.vector())
        return result

    def reset(self, use_snes=True, max_iterations=14):
        PETScOptions.set("pc_factor_mat_solver_package", "mumps")
        #PETScOptions.set("pc_factor_mat_solver_package", "superlu_dist")
        PETScOptions.set("ksp_type", "preonly")
        PETScOptions.set("mat_mumps_icntl_7", 6)
        if use_snes :
            solver = PETScSNESSolver()
            solver.parameters["report"] = False
            PETScOptions.set("snes_monitor")
        else :
            solver = NewtonSolver()
            
        solver.parameters["linear_solver"] = "lu"
        solver.parameters["maximum_iterations"] = max_iterations
        solver.parameters["lu_solver"]["same_nonzero_pattern"] = True
        solver.parameters["lu_solver"]["symmetric"] = True
        self.solver = solver

