"""This module implements a base class for ellipsoidal
biventricular geometries
"""
# Copyright (C) 2014-2015 Henrik Finsberg
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from fenicshotools import geo2dolfin
from . import BiVGeometry

import math as m
from textwrap import dedent
from os.path import isfile

# NOTE: THIS IS NOT TESTED
# THE ONLY THING DIFFERENT FOR LVEllipsoid is
# _compile_geo_body_code()

class BiVEllipsoid(BiVGeometry) :
    """
    Ellipsoidal (and Prolate) LV Geometry.
    """

    def __init__(self, h5name='', h5group='', comm=None,
                 regenerate_mesh=False, parameters=None) :
        """
        Create a LVEllipsoid from the parameters.
        """

        comm = comm if comm is not None else mpi_comm_world()

        # mesh generation occurs only when explicitely requested
        if regenerate_mesh:
            # set up the geometry parameters
            parameters = parameters or {}
            self._parameters = self.default_parameters()
            self._parameters.update(parameters)
            self.check_geometry_parameters()
            # geometry
            domain, names = self._generate_mesh(comm)
            # microstructure
            f0, s0, n0 = self._generate_microstructure(domain)
        else :
            # load from file
            domain, names, f0, s0, n0 = \
                super(LVEllipsoid, self)._load_from_file(comm, h5name, h5group)
            # parameters
            parameters = parameters or {}
            p = self.default_parameters()
            p.update(parameters)

            h5file = HDF5File(comm, h5name, "r")
            ggroup = "{}/geometry".format(h5group)
            p["axisymmetric"] = h5file.attributes(ggroup)["axisymmetric"]
            for a in p['geometry'].keys() :
                p["geometry"][a] = h5file.attributes(ggroup)[a]
            h5file.close()

            self._parameters = p
            self.check_geometry_parameters()

            # check for microstructure
            microp = self._parameters['microstructure']
            if microp['analytic_expression']:
                # we discard microstructure loaded from file
                f0, s0, n0 = self._generate_microstructure(domain)

        # initialize the parent class
        super(BiVEllipsoid, self).__init__(domain, names, f0, s0, n0, 'x')

        # save if necessary
        if h5name != '' and regenerate_mesh :
            self.save(h5name, h5group)

    @classmethod
    def from_file(cls, h5name, h5group='', comm=None) :

        comm = comm if comm is not None else mpi_comm_world()

        return cls(h5name, h5group, comm, False)

    @staticmethod
    def default_parameters() :
        p = { 'axisymmetric' : False,
              'geometry' : None,
              'mesh_generation' : {
                  'ndiv'  : 2,
                  'psize' : 1.0,
                  'order' : 1 },
              'microstructure' : {
                  'alpha_endo' : - 60.0,
                  'alpha_epi'  : + 60.0,
                  'beta_endo'  : - 0.0,
                  'beta_epi'   : + 0.0,
                  'interpolate_at_nodes' : False,
                  'analytic_expression' : False,
                  'fiber_at_quadrature_degree' : 4 } }
        return p

    def is_axisymmetric(self) :
        return self._parameters['axisymmetric']

    def save(self, h5name, h5group = '') :
        # save general data
        super(BiVEllipsoid, self).save(h5name, h5group)
        # save additional class informations
        p = self._parameters
        h5file = HDF5File(self.mesh.mpi_comm(), h5name, 'a')
        ggroup = '{}/geometry'.format(h5group)
        h5file.attributes(ggroup)['axisymmetric'] = p["axisymmetric"]
        for a in p['geometry'].keys() :
            h5file.attributes(ggroup)[a] = p["geometry"][a]
        h5file.close()

    def get_apex_position(self, surf = 'endo', u = None) :
        marker = { k: v for v, k in self.mesh.domains().markers(0).items() }
        if surf == 'endo' :
            idx = marker[self.ENDOPT]
        elif surf == 'epi' :
            idx = marker[self.EPIPT]
        else :
            raise ValueError

        X = self.mesh.coordinates()[idx, :]
        if u :
            return X[0] + u(X)[0]
        else :
            return X[0]

    def inner_volume_form(self, u = None) :
        if self.is_axisymmetric() :
            # all integrals are transformed in cylindrical coordinate
            # the section is such that T = 0.0 (=> X_3 = 0.0)
            #   X_1 = Z
            #   X_2 = R cos T
            #   X_3 = R sin T
            dom = self.domain

            xshift = [ 0.0, 0.0 ]
            xshift[self._long_axis] = self._endoring_offset
            xshift = Constant(tuple(xshift))

            X = SpatialCoordinate(dom) - xshift
            N = FacetNormal(dom)
            
            Z, R = X[0], X[1]
            N = as_vector([ N[0], N[1], 0.0 ])

            Jgeo = 2.0 * DOLFIN_PI * R

            u = u or Constant((0.0, 0.0, 0.0), cell = dom)

            # here we suppose tha
            z, r, t = Z + u[0], R + u[1], u[2]
            # deformation gradient tensor
            F = as_tensor([[ z.dx(0), z.dx(1), 0.0 ],
                           [ r.dx(0), r.dx(1), 0.0 ],
                           [ t.dx(0), t.dx(1), 1.0 ]])
            # normalization of components
            F = diag(as_vector([ 1, 1, r ])) * F
            F = F * diag(as_vector([ 1, 1, 1/R ]))

            x = as_vector([z, r, t])
            n = cofac(F)*as_vector([N[0], N[1], 0.0])

            return -1.0/3.0 * Jgeo * inner(x, n)
        else :
            return super(LVEllipsoid, self).inner_volume_form(u)

    def surface_area_form(self, u=None) :
        # full 3d can be handled by general form
        if not self.is_axisymmetric() :
            return super(LVEllipsoid, self).surface_area_form(u)

        # all integrals are transformed in cylindrical coordinate
        # the section is such that T = 0.0 (=> X_3 = 0.0)
        #   X_1 = Z
        #   X_2 = R cos T
        #   X_3 = R sin T
        dom = self.domain

        xshift = [ 0.0, 0.0 ]
        xshift[self._long_axis] = self._endoring_offset
        xshift = Constant(tuple(xshift))

        X = SpatialCoordinate(dom) - xshift
        N = FacetNormal(dom)
        
        Z, R = X[0], X[1]
        N = as_vector([ N[0], N[1], 0.0 ])

        Jgeo = 2.0 * DOLFIN_PI * R

        u = u or Constant((0.0, 0.0, 0.0), cell = dom)

        # here we suppose tha
        z, r, t = Z + u[0], R + u[1], u[2]
        # deformation gradient tensor
        F = as_tensor([[ z.dx(0), z.dx(1), 0.0 ],
                       [ r.dx(0), r.dx(1), 0.0 ],
                       [ t.dx(0), t.dx(1), 1.0 ]])
        # normalization of components
        F = diag(as_vector([ 1, 1, r ])) * F
        F = F * diag(as_vector([ 1, 1, 1/R ]))

        x = as_vector([z, r, t])
        n = cofac(F)*as_vector([N[0], N[1], 0.0])

        return Jgeo * sqrt(inner(n, n))

    def _compile_geo_body_code(self) :
        """
        Gmsh code template for a generic ellipsoid.
        """

        return dedent(\
        """\
        psize_ref = {mesh_generation[psize]} / {mesh_generation[ndiv]};
        axisymmetric = {axisymmetric:^};

        Geometry.CopyMeshingMethod = 1;
        Mesh.ElementOrder = {mesh_generation[order]};
        Mesh.Optimize = 1;
        Mesh.OptimizeNetgen = 1;
        Mesh.HighOrderOptimize = 1;

        Function EllipsoidPoint
            Point(id) = {{ r_long  * Cos(mu),
                           r_short * Sin(mu) * Cos(theta),
                           r_short * Sin(mu) * Sin(theta), psize }};
        Return

        center = newp; Point(center) = {{ 0.0, 0.0, 0.0 }};

        theta = 0.0;

        r_short_endo_lv = d_focal * Sinh(l_endo_lv);
        r_short_epi_lv  = d_focal * Sinh(l_epi_lv);
        r_long_endo_lv  = d_focal * Cosh(l_endo_lv);
        r_long_epi_lv  = d_focal * Cosh(l_epi_lv);

        r_short = r_short_endo_lv; r_long = r_long_endo_lv;
        mu = 0.0;
        psize = psize_ref / 2.0;
        apex_endo_lv = newp; id = apex_endo_lv; Call EllipsoidPoint;
        mu = mu_base;
        psize = psize_ref;
        base_endo_lv = newp; id = base_endo_lv; Call EllipsoidPoint;

        r_short = r_short_epi_lv; r_long = r_long_epi_lv;
        mu = 0.0;
        psize = psize_ref / 2.0;
        apex_epi_lv = newp; id = apex_epi_lv; Call EllipsoidPoint;
        mu = 45.0 / 180.0 * Pi;
        epi_mid1_lv = newp; id = epi_mid1_lv; Call EllipsoidPoint;
        mu = 60.0 / 180.0 * Pi;
        epi_mid2_lv = newp; id = epi_mid2_lv; Call EllipsoidPoint;

        center_rv = newp; Point(center_rv) = { 0.0, 3.0, 0.0 };
        endo_mid_rv = newp; Point(endo_mid_rv) = { 0.0, 4.0, 0.0 };
        base_endo_rv = newp; Point(base_endo_rv) = { -2.0, 3.8, 0.0 };
        epi_mid_rv = newp; Point(epi_mid_rv) = { 0.0, 5.0, 0.0 };
        base_epi_rv = newp; Point(base_epi_rv) = { -2.0, 4.8, 0.0 };

        mu = Acos(r_long_endo_lv / r_long_epi_lv * Cos(mu_base));
        psize = psize_ref;
        base_epi_lv = newp; id = base_epi_lv; Call EllipsoidPoint;

        apex_lv = newl; Line(apex_lv) = { apex_endo_lv, apex_epi_lv };
        base_lv = newl; Line(base_lv) = { base_endo_lv, base_epi_lv };
        endo_lv = newl; Ellipse(endo_lv) = { apex_endo_lv, center, apex_endo_lv, base_endo_lv };

        epi_lv_lower  = newl; Ellipse(epi_lv_lower) = { apex_epi_lv, center, apex_epi_lv, epi_mid1_lv };
        //epi_lv_mid  = newl; Ellipse(epi_lv_mid) = { epi_mid1_lv, center, apex_epi_lv, epi_mid2_lv };
        epi_lv_upper  = newl; Ellipse(epi_lv_upper) = { epi_mid2_lv, center, apex_epi_lv, base_epi_lv };

        endo_rv  = newl; Ellipse(endo_rv) = { epi_mid2_lv, center_rv, endo_mid_rv, base_endo_rv };
        epi_rv  = newl; Ellipse(epi_rv) = { epi_mid1_lv, center_rv, epi_mid_rv, base_epi_rv };
        base_rv = newl; Line(base_rv) = { base_endo_rv, base_epi_rv };

        ll1 = newll; Line Loop(ll1) = { apex_lv, epi_lv_lower, epi_rv, -base_rv, -endo_rv, epi_lv_upper, -base_lv, -endo_lv };
        s1 = news; Plane Surface(s1) = { ll1 };


        If (axisymmetric == 0)
            sendoringlvlist[] = { };
            sendoringrvlist[] = { };
            sepiringlvlist[]  = { };
            sepiringrvlist[]  = { };
            sendolv1list[] = { };
            sbaselvlist[] = { };
            sendorv2list[] = { };
            sendorv1list[] = { };
            sbaservlist[] = { };
            sepirvlist[] = { };
            sepilvlist[]  = { };

            sold = s1;
            For i In { 0 : 3 }
                out[] = Extrude { { 1.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, Pi/2 }
                                { Surface{sold}; };
                sendolvlist[i] = out[8];
                sbaselvlist[i] = out[7];
                sendorv2list[i] = out[6];
                sendorv1list[i] = out[5];
                sbaservlist[i] = out[4];
                sepirvlist[i] = out[3];
                sepilvlist[i]  = out[2];
                vlist[i] = out[1];
                bout1[] = Boundary{ Surface{ sbaselvlist[i] }; };
                sendoringlvlist[i] = bout1[1];
                sepiringlvlist[i] = bout1[3];
                bout2[] = Boundary{ Surface{ sbaservlist[i] }; };
                sepiringrvlist[i] = bout2[3];
                sold = out[0];
            EndFor

            Physical Volume("MYOCARDIUM") = { vlist[] };
            Physical Surface("ENDO_LV") = { sendolvlist[] };
            Physical Surface("ENDO_RV") = { sendorv1list[], sendorv2list[] };
            Physical Surface("EPI") = { sepilvlist[], sepirvlist[] };
            Physical Surface("BASE") = { sbaselvlist[], sbaservlist[] };
            Physical Line("ENDORING_LV") = { sendoringlvlist[] };
            Physical Line("ENDORING_RV") = { sendoringrvlist[] };
            Physical Line("ENDORING_RV_INNER") = { sepiringlvlist[]  }; 
            Physical Line("EPIRING") = { sepiringrvlist[] };

        EndIf
        If (axisymmetric == 1)
            Physical Surface("MYOCARDIUM") = { s1 };
            Physical Line("ENDO_LV") = { endo_lv };
            Physical Line("ENDO_RV") = { endo_rv, epi_lv_upper };
            Physical Line("EPI") = { epi_lv_lower, epi_rv };
            Physical Line("BASE") = { base_lv, base_rv };
            Physical Line("APEX") = { apex_lv };
            Physical Point("ENDORING_LV") = { base_endo_lv };
            Physical Point("ENDORING_RV") = { base_endo_rv, base_epi_lv };
            Physical Point("EPIRING") = { base_epi_rv };
        EndIf
        Physical Point("ENDOPT") = { apex_endo_lv };
        Physical Point("EPIPT") = { apex_epi_lv };
        """).format(**self._parameters)

    def _compile_microstructure_code(self) :
        """
        C++ code for analytic fiber and sheet.
        """
        pos = self._normalized_transmural_position_code()
        code = dedent(\
        """\
        #include <Eigen/Dense>

        class EllipsoidMicrostructure : public Expression
        {{
        public :

            typedef Eigen::Vector3d vec_type;
            typedef Eigen::Matrix3d mat_type;

            std::shared_ptr<dolfin::Expression> cart2coords;
            std::shared_ptr<dolfin::Expression> localbase;

            double alpha_epi, alpha_endo;
            double beta_epi,  beta_endo;

            EllipsoidMicrostructure() : Expression(3, 3),
                alpha_epi(0.0), alpha_endo(0.0),
                beta_epi(0.0), beta_endo(0.0)
            {{}}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // check if coordinates are ok
                assert(this->localbase);
                assert(this->cart2coords);

                // first find (lambda, mu, theta) from (x0, x1, x2)
                dolfin::Array<double> coords(3);
                this->cart2coords->eval(coords, raw_x, cell);

                // then evaluate the local basis
                dolfin::Array<double> base(9);
                this->localbase->eval(base, raw_x, cell);

                // transmural position
                double pos = 0.0;
                {pos}

                // angles
                double alpha = (alpha_epi - alpha_endo) * pos + alpha_endo;
                double beta  = (beta_epi - beta_endo) * pos + beta_endo;
                alpha = alpha / 180.0 * M_PI;
                beta  = beta  / 180.0 * M_PI;

                // Each column is a basis vector
                // --> [ e_lambda, e_mu, e_theta ]
                mat_type S = Eigen::Map<mat_type>(base.data());

                // Rotation around e_lambda of angle alpha
                Eigen::AngleAxisd rot1(alpha, S.col(0));
                S = rot1 * S;
                // --> [ e_lambda, _, f0 ]

                // Rotation around f0 of angle beta
                Eigen::AngleAxisd rot2(beta, S.col(2));
                S = rot2 * S;
                // --> [ s0, n0, f0 ]

                // Return the values
                Eigen::Map<mat_type>(values.data()) = S;
            }}
        }};
        """).format(pos=pos, **self._parameters['microstructure'])

        return code

    def _generate_mesh(self, comm) :
        """
        Mesh generated by Gmsh from .geo code.
        """
        p = self._parameters

        # generate the mesh from geo code
        code = self._compile_geo_code()
        if p['axisymmetric'] :
            mdim = 2
            gdim = 2
        else :
            mdim = 3
            gdim = 3
        domain, names = geo2dolfin(code, mdim, gdim, comm)

        return domain, names

    def _generate_microstructure(self, domain) :
        """
        Build or interpolate microstructure.
        """
        p = self._parameters
        code = self._compile_microstructure_code()

        # select the space depending on the type
        order = p['mesh_generation']['order']
        if p['microstructure']['interpolate_at_nodes'] :
            Tspace = TensorFunctionSpace(domain, "P", order, shape = (3, 3))
            S = VectorFunctionSpace(domain, "P", order, 3)
        else :
            degree = p['microstructure']['fiber_at_quadrature_degree']
            Tspace = TensorFunctionSpace(domain, "Quadrature", degree,
                                         shape = (3, 3))
            S = VectorFunctionSpace(domain, "Quadrature", degree, 3)

        # coordinate mapper
        coords = Expression(cppcode = self._compile_cart2coords_code())
        if order > 1 :
            coords.coords = domain.coordinates()

        # local basis
        localbase = Expression(cppcode = self._compile_localbase_code())
        localbase.cart2coords = coords

        Texpr = Expression(cppcode = code,
                    element = Tspace.ufl_element(),
                    alpha_endo = p['microstructure']['alpha_endo'],
                    alpha_epi = p['microstructure']['alpha_epi'],
                    beta_endo = p['microstructure']['beta_endo'],
                    beta_epi = p['microstructure']['beta_epi'])
        Texpr.cart2coords = coords
        Texpr.localbase = localbase

        if p['microstructure']['analytic_expression'] :
            s0, n0, f0 = Texpr[0, :], Texpr[1, :], Texpr[2, :]
        else :
            Tinterp = interpolate(Texpr, Tspace)
            s0 = Function(S, name = "s0")
            n0 = Function(S, name = "n0")
            f0 = Function(S, name = "f0")
            assign(s0, [Tinterp.sub(0), Tinterp.sub(1), Tinterp.sub(2)])
            assign(n0, [Tinterp.sub(3), Tinterp.sub(4), Tinterp.sub(5)])
            assign(f0, [Tinterp.sub(6), Tinterp.sub(7), Tinterp.sub(8)])

        return f0, s0, n0

