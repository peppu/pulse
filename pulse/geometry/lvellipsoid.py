"""This module implements a base class for ellipsoidal single
ventricular geometries
"""
# Copyright (C) 2014-2015 Simone Pezzuto
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.

import math as m
from textwrap import dedent
from os.path import isfile

from dolfin import *
from fenicshotools import geo2dolfin
from .lvgeometry import LVGeometry

class LVEllipsoid(LVGeometry):
    """
    Ellipsoidal (and Prolate) LV Geometry.
    """

    def __init__(self,h5name='',h5group='',comm=None,regenerate_mesh=False,
                 parameters=None) :
        """
        Create a LVEllipsoid from the parameters.
        """

        comm = comm if comm is not None else mpi_comm_world()

        # mesh generation occurs only when explicitely requested
        if not isfile(h5name) or regenerate_mesh:
            log(PROGRESS, "LVEllipsoid: rebuilding geometry")
            # set up the geometry parameters
            parameters = parameters or {}
            self._parameters = self.default_parameters()
            self._parameters.update(parameters)
            self.check_geometry_parameters()

            # geometry
            mesh,phi,names = self._generate_mesh(comm)
            if phi is not None:
                domain = create_mesh(phi)
            else:
                domain = mesh

            # microstructure
            f0,s0,n0 = self._generate_microstructure(domain,mesh,phi)

        else:
            # load from file
            domain,mesh,phi,names,f0,s0,n0 = \
                super(LVEllipsoid,self)._load_from_file(comm,h5name,h5group)
            # parameters
            log(PROGRESS, "LVEllipsoid: setting parameters from file")
            parameters = parameters or {}
            p = self.default_parameters()
            p.update(parameters)

            with HDF5File(comm, h5name, "r") as h5file :
                ggroup = "{}/geometry".format(h5group)
                attrs = h5file.attributes(ggroup)
                if h5file.has_dataset(ggroup) and "axisymmetric" in attrs:
                    p["axisymmetric"] = attrs["axisymmetric"]
                for a in p['geometry'].keys():
                    if a in attrs:
                        p["geometry"][a] = attrs[a]

            self._parameters = p
            self.check_geometry_parameters()

            log(PROGRESS, "LVEllipsoid: checking microstructure from file")

            # Assume excisting microstructure and use it.
            # check for microstructure
            #if self._parameters['microstructure']['interpolate'] :
            #
            #    # we discard microstructure loaded from file
            #    f0, s0, n0 = self._generate_microstructure(domain)

        # initialize the parent class
        super(LVEllipsoid,self).__init__(domain,mesh,phi,names,f0,s0,n0,'x')

        # save if h5name was given
        if h5name != '' and regenerate_mesh:
            self.save(h5name,h5group)

    @classmethod
    def from_file(cls,h5name,h5group='',comm=None) :
        comm = comm if comm is not None else mpi_comm_world()

        return cls(h5name,h5group,comm,False)

    @staticmethod
    def default_parameters() :
        p = { 'axisymmetric' : False,
              'geometry' : None,
              'mesh_generation' : {
                  'ndiv'  : 2,
                  'psize' : 1.0,
                  'order' : 1 },
              'microstructure' : {
                  'alpha_endo' : - 60.0,
                  'alpha_epi'  : + 60.0,
                  'beta_endo'  : - 0.0,
                  'beta_epi'   : + 0.0,
                  'function_space' : 'Quadrature_4',
                  'interpolate' : True } }
        return p

    def is_axisymmetric(self) :
        return self._parameters['axisymmetric']

    def save(self,h5name,h5group=''):
        # save general data
        super(LVEllipsoid,self).save(h5name,h5group)

        # save additional class informations
        p = self._parameters
        h5file = HDF5File(self.mesh.mpi_comm(), h5name, 'a')
        ggroup = '{}/geometry'.format(h5group)
        h5file.attributes(ggroup)['axisymmetric'] = p["axisymmetric"]
        for a in p['geometry'].keys() :
            h5file.attributes(ggroup)[a] = p["geometry"][a]
        h5file.close()

    def get_apex_position(self, surf='endo', u=None):
        marker = { k: v for v, k in self.mesh.domains().markers(0).items() }
        if surf == 'endo' :
            idx = marker[self.ENDOPT]
        elif surf == 'epi' :
            idx = marker[self.EPIPT]
        else :
            raise ValueError

        X = self.mesh.coordinates()[idx, :]
        if u :
            return X[0] + u(X)[0]
        else :
            return X[0]

    def deformation_gradient(self,u=None):
        if self.is_axisymmetric():
            dom = self.domain

            X = SpatialCoordinate(dom)
            Z,R = X[0],X[1]

            u = u or Constant((0.0,0.0,0.0),cell=dom.ufl_cell())

            z,r,t = Z+u[0],R+u[1],u[2]
            F = as_tensor([[ z.dx(0),z.dx(1),0.0 ],
                           [ r.dx(0),r.dx(1),0.0 ],
                           [ t.dx(0),t.dx(1),1.0 ]])
            F = diag(as_vector([ 1,1,r ])) * F
            F = F * diag(as_vector([ 1,1,1/R ]))

            return F
        else:
            return super(LVEllipsoid, self).deformation_gradient(u)


    def inner_volume_form(self,u=None):
        if self.is_axisymmetric():
            # all integrals are transformed in cylindrical coordinate
            # the section is such that T = 0.0 (=> X_3 = 0.0)
            #   X_1 = Z
            #   X_2 = R cos T
            #   X_3 = R sin T
            dom = self.domain

            xshift = [ 0.0,0.0 ]
            xshift[self._long_axis] = self._endoring_offset
            xshift = Constant(tuple(xshift),cell=dom.ufl_cell())

            X = SpatialCoordinate(dom) - xshift
            N = FacetNormal(dom)

            Z,R = X[0],X[1]
            N = as_vector([ N[0],N[1],0.0 ])

            Jgeo = 2.0 * DOLFIN_PI * R

            u = u or Constant((0.0,0.0,0.0),cell=dom.ufl_cell())

            # here we suppose tha
            z,r,t = Z+u[0],R+u[1],u[2]
            # deformation gradient tensor
            F = as_tensor([[ z.dx(0),z.dx(1),0.0 ],
                           [ r.dx(0),r.dx(1),0.0 ],
                           [ t.dx(0),t.dx(1),1.0 ]])
            # normalization of components
            F = diag(as_vector([ 1,1,r ])) * F
            F = F * diag(as_vector([ 1,1,1/R ]))

            x = as_vector([z,r,t])
            n = cofac(F)*as_vector([N[0],N[1],0.0])

            return -1.0/3.0 * Jgeo * inner(x,n)
        else :
            return super(LVEllipsoid, self).inner_volume_form(u)

    def surface_area_form(self, u=None):
        # full 3d can be handled by general form
        if not self.is_axisymmetric() :
            return super(LVEllipsoid, self).surface_area_form(u)

        # all integrals are transformed in cylindrical coordinate
        # the section is such that T = 0.0 (=> X_3 = 0.0)
        #   X_1 = Z
        #   X_2 = R cos T
        #   X_3 = R sin T
        dom = self.domain

        xshift = [ 0.0, 0.0 ]
        xshift[self._long_axis] = self._endoring_offset
        xshift = Constant(tuple(xshift),cell=dom.ufl_cell())

        X = SpatialCoordinate(dom) - xshift
        N = FacetNormal(dom)

        Z, R = X[0], X[1]
        N = as_vector([ N[0], N[1], 0.0 ])

        Jgeo = 2.0 * DOLFIN_PI * R

        u = u or Constant((0.0, 0.0, 0.0), cell=dom)

        # here we suppose tha
        z, r, t = Z + u[0], R + u[1], u[2]
        # deformation gradient tensor
        F = as_tensor([[ z.dx(0), z.dx(1), 0.0 ],
                       [ r.dx(0), r.dx(1), 0.0 ],
                       [ t.dx(0), t.dx(1), 1.0 ]])
        # normalization of components
        F = diag(as_vector([ 1, 1, r ])) * F
        F = F * diag(as_vector([ 1, 1, 1/R ]))

        x = as_vector([z, r, t])
        n = cofac(F)*as_vector([N[0], N[1], 0.0])

        return Jgeo * sqrt(inner(n, n))

    def _compile_geo_body_code(self):
        """
        Gmsh code template for a generic ellipsoid.
        """

        return dedent(\
        """\
        psize_ref = {mesh_generation[psize]} / {mesh_generation[ndiv]};
        axisymmetric = {axisymmetric:^};

        Geometry.CopyMeshingMethod = 1;
        Mesh.ElementOrder = {mesh_generation[order]};
        Mesh.Optimize = 1;
        Mesh.OptimizeNetgen = 1;
        Mesh.HighOrderOptimize = 1;

        Function EllipsoidPoint
            Point(id) = {{ r_long  * Cos(mu),
                           r_short * Sin(mu) * Cos(theta),
                           r_short * Sin(mu) * Sin(theta), psize }};
        Return

        center = newp; Point(center) = {{ 0.0, 0.0, 0.0 }};

        theta = 0.0;

        r_short = r_short_endo; r_long = r_long_endo;
        mu = 0.0;
        psize = psize_ref / 2.0;
        apex_endo = newp; id = apex_endo; Call EllipsoidPoint;
        mu = mu_base;
        psize = psize_ref;
        base_endo = newp; id = base_endo; Call EllipsoidPoint;

        r_short = r_short_epi; r_long = r_long_epi;
        mu = 0.0;
        psize = psize_ref / 2.0;
        apex_epi = newp; id = apex_epi; Call EllipsoidPoint;
        mu = Acos(r_long_endo / r_long_epi * Cos(mu_base));
        psize = psize_ref;
        base_epi = newp; id = base_epi; Call EllipsoidPoint;

        apex = newl; Line(apex) = {{ apex_endo, apex_epi }};
        base = newl; Line(base) = {{ base_endo, base_epi }};
        endo = newl; Ellipse(endo) = {{ apex_endo, center, apex_endo, base_endo }};
        epi  = newl; Ellipse(epi) = {{ apex_epi, center, apex_epi, base_epi }};

        ll1 = newll; Line Loop(ll1) = {{ apex, epi, -base, -endo }};
        s1 = news; Plane Surface(s1) = {{ ll1 }};

        If (axisymmetric == 0)
            sendoringlist[] = {{ }};
            sepiringlist[]  = {{ }};
            sendolist[] = {{ }};
            sepilist[]  = {{ }};
            sbaselist[] = {{ }};
            vlist[] = {{ }};
            
            sold = s1;
            For i In {{ 0 : 3 }}
                out[] = Extrude {{ {{ 1.0, 0.0, 0.0 }}, {{ 0.0, 0.0, 0.0 }}, Pi/2 }}
                                {{ Surface{{sold}}; }};
                sendolist[i] = out[4];
                sepilist[i]  = out[2];
                sbaselist[i] = out[3];
                vlist[i] = out[1];
                bout[] = Boundary{{ Surface{{ sbaselist[i] }}; }};
                sendoringlist[i] = bout[1];
                sepiringlist[i] = bout[3];
                sold = out[0];
            EndFor

            Physical Volume("MYOCARDIUM") = {{ vlist[] }};
            Physical Surface("ENDO") = {{ sendolist[] }};
            Physical Surface("EPI") = {{ sepilist[] }};
            Physical Surface("BASE") = {{ sbaselist[] }};
            Physical Line("ENDORING") = {{ sendoringlist[] }};
            Physical Line("EPIRING") = {{ sepiringlist[] }};
        EndIf
        If (axisymmetric == 1)
            Physical Surface("MYOCARDIUM") = {{ s1 }};
            Physical Line("ENDO") = {{ endo }};
            Physical Line("EPI") = {{ epi }};
            Physical Line("BASE") = {{ base }};
            Physical Line("APEX") = {{ apex }};
            Physical Point("ENDORING") = {{ base_endo }};
            Physical Point("EPIRING") = {{ base_epi }};
        EndIf
        Physical Point("ENDOPT") = {{ apex_endo }};
        Physical Point("EPIPT") = {{ apex_epi }};
        """).format(**self._parameters)

    def _compile_microstructure_code(self):
        """
        C++ code for analytic fiber and sheet.
        """
        pos = self._normalized_transmural_position_code()
        code = dedent(\
        """\
        #include <Eigen/Dense>

        namespace dolfin {{

        class EllipsoidMicrostructure : public Expression
        {{
        public :

            typedef Eigen::Vector3d vec_type;
            typedef Eigen::Matrix3d mat_type;

            std::shared_ptr<dolfin::Expression> cart2coords;
            std::shared_ptr<dolfin::Expression> localbase;

            double alpha_epi, alpha_endo;
            double beta_epi,  beta_endo;

            EllipsoidMicrostructure() : Expression(3, 3),
                alpha_epi(0.0), alpha_endo(0.0),
                beta_epi(0.0), beta_endo(0.0)
            {{}}

            void eval(dolfin::Array<double>& values,
                      const dolfin::Array<double>& raw_x,
                      const ufc::cell& cell) const
            {{
                // check if coordinates are ok
                assert(this->localbase);
                assert(this->cart2coords);

                // first find (lambda, mu, theta) from (x0, x1, x2)
                dolfin::Array<double> coords(3);
                this->cart2coords->eval(coords, raw_x, cell);

                // then evaluate the local basis
                dolfin::Array<double> base(9);
                this->localbase->eval(base, raw_x, cell);

                // transmural position
                double pos = 0.0;
                {pos}

                // angles
                double alpha = (alpha_epi - alpha_endo) * pos + alpha_endo;
                double beta  = (beta_epi - beta_endo) * pos + beta_endo;
                alpha = alpha / 180.0 * M_PI;
                beta  = beta  / 180.0 * M_PI;

                // Each column is a basis vector
                // --> [ e_lambda, e_mu, e_theta ]
                mat_type S = Eigen::Map<mat_type>(base.data());

                // Rotation around e_lambda of angle alpha
                Eigen::AngleAxisd rot1(alpha, S.col(0));
                S = rot1 * S;
                // --> [ e_lambda, _, f0 ]

                // Rotation around f0 of angle beta
                Eigen::AngleAxisd rot2(beta, S.col(2));
                S = rot2 * S;
                // --> [ s0, n0, f0 ]

                // Return the values
                Eigen::Map<mat_type>(values.data()) = S;
            }}
        }};

        }};
        """).format(pos=pos, **self._parameters['microstructure'])

        return code

    def _generate_mesh(self,comm):
        """
        Mesh generated by Gmsh from .geo code.
        """
        p = self._parameters

        # generate the mesh from geo code
        code = self._compile_geo_code()
        if p['axisymmetric'] :
            mdim = 2
            gdim = 2
        else :
            mdim = 3
            gdim = 3
        mesh,phi,names = geo2dolfin(code,mdim,gdim,comm)

        return mesh,phi,names

    def _generate_microstructure(self,domain,mesh,phi,force_interp=False):
        """
        Build or interpolate microstructure.
        """

        p = self._parameters
        order = p['mesh_generation']['order']
        space = p['microstructure']['function_space']
        interp = p['microstructure']['interpolate']

        # the underlying FE space for the microstructure
        code = self._compile_microstructure_code()
        family,degree = space.split("_")
        fe = FiniteElement(family,domain.ufl_cell(),
                    int(degree),quad_scheme="default")
        vfe = VectorElement(fe,dim=3)
        tfe = TensorElement(fe,shape=(3,3))

        # local coordinates and base
        code = self._compile_cart2coords_code()
        coords = Expression(cppcode=code,element=vfe)
        if order > 1:
            coords.coords = phi
        code = self._compile_localbase_code()
        localbase = Expression(cppcode=code,element=tfe)
        localbase.cart2coords = coords

        # compilation of the expression
        Tspace = FunctionSpace(domain,tfe)
        code = self._compile_microstructure_code()
        Texpr = Expression(cppcode=code,element=tfe,
                           alpha_endo=p['microstructure']['alpha_endo'],
                           alpha_epi=p['microstructure']['alpha_epi'],
                           beta_endo=p['microstructure']['beta_endo'],
                           beta_epi=p['microstructure']['beta_epi'])
        Texpr.cart2coords = coords
        Texpr.localbase = localbase

        # interpolate if requested
        if interp or force_interp:
            vfe = VectorElement(fe,dim=3)
            Vspace = FunctionSpace(domain,vfe)
            T = interpolate(Texpr,Tspace)
            s0 = Function(Vspace,name="s0")
            n0 = Function(Vspace,name="n0")
            f0 = Function(Vspace,name="f0")
            assign(s0, [T.sub(0),T.sub(1),T.sub(2)])
            assign(n0, [T.sub(3),T.sub(4),T.sub(5)])
            assign(f0, [T.sub(6),T.sub(7),T.sub(8)])
        else:
            s0,n0,f0 = Texpr[0,:],Texpr[1,:],Texpr[2,:]

        return f0,s0,n0

