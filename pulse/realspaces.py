"""This module hsould be removed..."""
from fenics import *

def get_realspace_var(u, num) :
    value = Function(FunctionSpace(u.function_space().mesh(), "R", 0))
    assign(value, u.sub(num))
    return float(value)

def set_realspace_var(u, num, val) :
    value = Function(FunctionSpace(u.function_space().mesh(), "R", 0))
    value.interpolate(Constant(val))
    assign(u.sub(num), value)
