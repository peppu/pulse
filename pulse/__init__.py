
# Set form compiler representation to uflacs
import dolfin
dolfin.parameters['form_compiler']['representation'] = 'uflacs'

from pulse.geometry import *
from pulse.material import *
from pulse.itertarget import *
from pulse.lvproblem import *
from pulse.bivproblem import *
from pulse.lvsolver import *
from pulse.postprocess import *
from pulse.datacollector import *

