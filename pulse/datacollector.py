"""This module implements a data collector for collection of data from an LVProblem 
"""
# Copyright (C) 2014-2015 Johan Hake
#
# This file is part of PULSE.
#
# PULSE is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE. If not, see <http://www.gnu.org/licenses/>.
#

from dolfin import *
from six.moves import cPickle
import numpy as np
import os

__all__ = ["DataCollector"]

class DataCollector(object) :

    def __init__(self, outdir, problem, reuse_database=False):

        self.problem = problem
        self.outdir = outdir

        # All relevant parameters
        self.plist = ["pressure", "volume", "gamma"]

        # database
        # --------
        comm = problem.geo.mesh.mpi_comm()
        dbfile = "{}/state.h5".format(outdir)
        ppfile = "{}/param.cpickle".format(outdir)
        if os.path.isfile(dbfile) and reuse_database:
            # open in append mode
            self.db = HDF5File(comm, dbfile, "a")
            self.pp = cPickle.load(open(ppfile, "r"))
        else:
            # create a new one
            self.db = HDF5File(comm, dbfile, "w")
            self.pp = []

    def print_status(self):
        problem = self.problem
        info('STATUS')
        for p in self.plist:
            if p in ["pressure", "volume"]:
                val = problem.get_control_parameter(p)
                info('--> {:<8} LV = {}'.format(p, val))
                try:
                    val = problem.get_control_parameter(p, vent="RV")
                    info('--> {:<8} RV = {}'.format(p, val))
                except:
                    pass
            else:
                val = problem.get_control_parameter(p)
                info('--> {:<8} = {}'.format(p, val))

    def save(self):
        problem = self.problem

        count = len(self.pp)
        mode = self.problem.get_control_mode()
        info('STORING SOLUTION {} ...'.format(count))

        # save all relevant parameters
        
        try:        
            nplist = ['pressure', 'pressure','volume', 'volume', 'gamma']
            ventlist = ['LV', 'RV', 'LV', 'RV', 'LV']
            self.pp.append(map(problem.get_control_parameter, nplist, ventlist))
        except:
            self.pp.append(map(problem.get_control_parameter, self.plist))
        
        self.pp[-1].append(mode)

        # store the state
        state = self.problem.get_state()
        if not self.db.has_dataset("/state_with_control_{}".format(mode)):
            # store one full solution
            self.db.write(state, "/state_with_control_{}".format(mode))

        # store the values anyway
        self.db.write(state.vector(), "/values_{}".format(count))

    def find_solution(self, **kwargs):
        query = ' & '.join('(abs({} - {}) < {})'.format(p, v, 1e-10)
                           for p, v in kwargs.items())
        idx = -1
        for row in self.ptable.where(query):
            idx = row.nrow
        return idx

    def flush(self):
        self.db.flush()
        ppfile = "{}/param.cpickle".format(self.outdir)
        cPickle.dump(self.pp, open(ppfile, "w"))

    def load(self, idx):
        info('RESTORING SOLUTION {} ...'.format(idx))

        if idx >= len(self.pp):
            error("Index {} out of range!".format(idx))

        problem = self.problem

        mode = self.problem.get_control_mode()
        param = dict(zip(self.plist + [ 'control_mode' ], self.pp[idx]))
        pmode = param['control_mode']

        if mode == pmode:
            state = problem.get_state()
            self.db.read(state.vector(), "/values_{}".format(idx), True)
        else:
            problem.set_control_mode(pmode)
            state = problem.get_state()
            self.db.read(state, "/state_with_control_{}".format(pmode))
            state = problem.get_state()
            self.db.read(state.vector(), "/values_{}".format(idx), True)

        problem.set_pendo(param['pressure'])
        problem.set_control_parameters(gamma = param['gamma'])
        if self.problem.get_control_mode() == 'volume':
            problem.set_Vendo(param['volume'])

    def plot(self):
        pass
        #u_out = Function(self.problem.get_state(), 0, name = 'displacement')
        #self.ufile << (u_out, float(count))
        #p_out = Function(self.problem.get_state(), 1, name = 'pressure')
        #self.pfile << (p_out, float(count))

    def interpolate_closest_solution(self, control, value):
        pass
