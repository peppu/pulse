parameters(
    V_fluid=ScalarParam(1.0, description="Volume of cavity fluid"),
    f_act=0.0)

states("Myocardium",
       lambda_f=ScalarParam(1., description="Fiber stretch"),
       lambda_s=ScalarParam(1., description="Sheet stretch")) 

parameters("Myocardium",
           a_iso =  0.02362,
           b_iso = 10.810,
           a_f   =  2.0037/10., #reducing stiffness to get higher ejection
           b_f   = 14.154,
           a_s   =  0.37245,
           b_s   =  5.1645,
           )

states("Cavity pressure",
       p_fluid=ScalarParam(0.00, description="Pressure of cavity fluid"),
       )

parameters("Cavity pressure",
           A0 = 1.0,
           l0 = 1.0,
           V_ref = 1.0,
           )

component("Myocardium")

comment("Microstructure")
f0 = Matrix([1, 0, 0])
s0 = Matrix([0, 1, 0])
n0 = Matrix([0, 0, 1])

comment("Deformation")
lambda_n = 1/(lambda_f*lambda_s)
    
F = lambda_f * f0*f0.transpose() + \
    lambda_s * s0*s0.transpose() + \
    lambda_n * n0*n0.transpose()
B = F*F.transpose()
C = F.transpose()*F

comment("Invariants")
I1  = C.trace()
I4f = (C*f0).dot(f0)
I4s = (C*s0).dot(s0)
    
comment("Strain-energy derivatives")
dW1  = a_iso/2 * exp(b_iso*(I1 - 3))
dW4f = a_f * Conditional(Gt(I4f - 1, 0), (I4f - 1)*exp(b_f*(I4f - 1)**2), 0) 
dW4s = a_s * Conditional(Gt(I4s - 1, 0), (I4s - 1)*exp(b_s*(I4s - 1)**2), 0)

comment("passive cauchy stress")
Tpas = 2.0 * dW1 * B + \
       2.0 * dW4f * (B*f0)*f0.transpose() + \
       2.0 * dW4s * (B*s0)*s0.transpose()
    
comment("active stress")
Tact = f_act * (B*f0)*f0.transpose()
    
comment("reaction constraint for fluid")
Text = p_fluid * F.det()*(F.inv()*s0) * s0.transpose()
    
# reaction constraint for incompressibility
# assumption: n0 direction stress-free
Tinc = - (Tpas*n0).dot(n0)*eye(3)
    
# stress balance
T = Tpas + Tact + Tinc + Text

# since n0 direction is stress-free, balance is given by
alg_lambda_f_0 = (T*f0).dot(f0)
alg_lambda_s_0 = (T*s0).dot(s0)

component("Cavity pressure")

# volume constraint (in cm3)

alg_p_fluid_0 = (1 - lambda_s)*A0*l0 + V_ref - V_fluid
