import sys
import os
from dolfin import *

def add_mesh_value_collection(mfun) :

    if mfun.empty() : return

    coll = MeshValueCollection("size_t", mfun)
    dim  = coll.dim()
    mesh = coll.mesh()
    mesh.init(dim)

    for (cell_id, local_entity), value in coll.values().iteritems() :
        # skip zero entries
        if value == 0 : continue
        # global entity_id
        cell = Cell(mesh, cell_id)
        entity_id = cell.entities(dim)[local_entity]
        mesh.domains().set_marker((entity_id, value), dim)


if __name__ == "__main__" :
  
    if len(sys.argv) < 3:
        sys.exit("Usage: %s [imesh] [omesh]" % sys.argv[0])
  
    set_log_level(DBG)
  
    mesh = Mesh(sys.argv[1] + ".xml")
    
    bfun = MeshFunction("size_t", mesh, sys.argv[1] + "_facet_region.xml")
    dfun = MeshFunction("size_t", mesh, sys.argv[1] + "_physical_region.xml")

    add_mesh_value_collection(bfun)
    #add_mesh_value_collection(dfun)
    
    # export
    File(sys.argv[2] + ".xml") << mesh
  
    # compressing
    os.system("gzip " + sys.argv[2] + ".xml")
