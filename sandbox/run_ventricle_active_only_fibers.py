"""
Activation only on W_4f, so the total energy is:

    W(F) = W_1(F) + W_4f(Fe) + W_4s(F) + W_8fs(F)

thus the non-fiber specific terms are only passive.
"""

import sys
from dolfin import *
from ventricle import *

# parameters
geometry_type = "prolate"
material_type = "holzapfelogden"
output_dir = "results/prolate_active_only_fibers"
postprocess = True

def main():

    if geometry_type == "disk" :
        from geometry import Disk
        geo = Disk()
    elif geometry_type == "prolate" :
        from geometry import Prolate
        geo = Prolate()
    else :
        return "geometry not implemented!"

    # material to use
    if material_type == "holzapfelogden" :
        from material import HolzapfelOgden
        mat = HolzapfelOgden(geo)
    else :
        return "material not implemented!"


    ventricle = Ventricle(geo, mat, output_dir, postprocess)
    ventricle.active_only_fibers = True

    if postprocess :
        from postprocess import ventricle_postprocess
        geo.construct_local_base()
        ventricle_postprocess(ventricle, output_dir)
    else :
        ventricle.cardiac_cycle()

    list_timings()

if __name__ == "__main__" :
    sys.exit(main())
