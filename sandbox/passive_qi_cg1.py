"""
Holzapfel-Ogden model, passive inflation.
Does quasi-incompressible CG1 model behave
correctly?

Results:
    - CG2/CG1, quad=4     --> ok
    - Quad=2, CG1 {u,f,s} --> rhombic deformation
    - kappa = 1e3 to 1e5  --> no deformation at all

"""

from dolfin import *

import sys
sys.path.append('../')
from dolfin import *
from geometry import Prolate
from material import HolzapfelOgden

ffc_options = \
{
    "quadrature_degree" : 2,
    "cpp_optimize" : True,
    "eliminates_zeros" : True,
    "precompute_basis_const" : True,
    "optimize" : True
}

class VentricleSolver() :

    def __init__(self, G, dG, w, geo, bcs) :

        self.geo = geo
        self.bcs = bcs
        self.dG = dG
        self.G = G
        self.w = w

        self.dw = self.w.copy()
        self.dw.zero()

        # data
        self.A = PETScMatrix()
        self.b = PETScVector()
        self.x = as_backend_type(self.dw)

        # init matrices
        assemble_system(self.dG, self.G, self.bcs, \
                    A_tensor = self.A, \
                    b_tensor = self.b, \
                    reset_sparsity = True, \
                    exterior_facet_domains = geo.bfun, \
                    form_compiler_parameters = ffc_options)

    def solve(self) :

        solver = PETScLUSolver("petsc")
        ksp = solver.ksp()
        ksp.setOptionsPrefix("tangent_")

        PETScOptions.set("tangent_ksp_type", "preonly")
        PETScOptions.set("tangent_pc_type", "lu")
        if MPI.size(mesh.mpi_comm()) > 1 :
            PETScOptions.set("tangent_pc_factor_mat_solver_package", "mumps")
            # ordering strategy
            PETScOptions.set("mat_mumps_icntl_7", 6)
            # verbosity level
            #PETScOptions.set("mat_mumps_icntl_4", 2)
        else:
            PETScOptions.set("tangent_pc_factor_mat_solver_package", "superlu")

        ksp.setFromOptions()

        nl_max_iter = 15
        nl_inc_toll = 1e-8
        nl_res_toll = 1e-8
        nl_conv = False

        for nl_iter in xrange(0, nl_max_iter) :

            # assemble of tangent problem
            assemble_system(self.dG, self.G, self.bcs, \
                            A_tensor = self.A, \
                            b_tensor = self.b, \
                            reset_sparsity = False, \
                            exterior_facet_domains = self.geo.bfun, \
                            form_compiler_parameters = ffc_options)
            
            # raw petsc objects
            A_petsc = self.A.mat()
            b_petsc = self.b.vec()
            x_petsc = self.x.vec()

            # pattern doesn't between iterations
            # FIXME should be set at timestep level
            struct = A_petsc.Structure.SAME_NONZERO_PATTERN

            # solve the linear problem
            ksp.setOperators(A_petsc, A_petsc, struct)
            ksp.solve(b_petsc, x_petsc)

            # update the state
            self.w.axpy(-1.0, self.dw)

            # computing errors
            err_inc = x_petsc.norm()
            err_res = b_petsc.norm()

            print("-- Newton iter %d, err_inc %e, err_res %e" 
                    % (nl_iter, err_inc, err_res))

            if (err_inc < nl_inc_toll) and (err_res < nl_inc_toll) :
                print("-- Newton converged")
                nl_conv = True
                break


if __name__ == "__main__" :

    quasi_inc = True

    geo = Prolate()
    geo.construct_mesh("../meshes/prolate")

    mat = HolzapfelOgden(geo)

    # copying from ventricle.py
    mesh = geo.mesh
    
    Rvec = VectorFunctionSpace(mesh, "Real", 0, 3)
    if quasi_inc :
        u_space = VectorFunctionSpace(mesh, "CG", 1)
        mech_space = MixedFunctionSpace([u_space, Rvec])
    else :
        u_space = VectorFunctionSpace(mesh, "CG", 2)
        p_space = FunctionSpace(mesh, "CG", 1)
        mech_space = MixedFunctionSpace([u_space, p_space, Rvec])

    # fibers and sheets space
    fs_space = VectorFunctionSpace(mesh, "Quadrature", ffc_options["quadrature_degree"])

    # microstructure
    mat.fibers = Expression(cppcode = geo.fibers, \
                            element = fs_space.ufl_element())

    mat.sheets = Expression(cppcode = geo.sheets, \
                            element = fs_space.ufl_element())

    # the problem
    mech_state = Function(mech_space)
    if quasi_inc :
        u, c = split(mech_state)
    else :
        u, p, c = split(mech_state)

    p_endo = Constant(0.0)

    I = Identity(geo.dim)
    F = I + grad(u)
    C = F.T * F
    J = det(F)

    if quasi_inc :
        kappa = Constant(35.0) # N/cm2
        # quasi-incompressible formulation
        Jm23 = pow(J, -float(2)/3)
        Cbar = Jm23 * C
        W_iso = mat.strain_energy(Cbar) * dx
        W_vol = kappa/4.0 * (J**2 - 1.0 - 2.0*ln(J)) * dx
    else :
        W_iso = mat.strain_energy(C) * dx
        W_vol = p * (J - 1) * dx

    # motion constraints
    X = SpatialCoordinate(geo.mesh.ufl_domain())
    N = FacetNormal(geo.mesh.ufl_domain())
    W_bcs = inner(as_vector([c[0], c[1], 0.0]), u) * dx \
          + inner(c[2], cross(X, u)[2]) * dx
    Vbc = mech_space.sub(0).sub(2)
    bcs = DirichletBC(Vbc, Constant(0.0), geo.bfun, geo.BASE)

    # external load
    W_ext = p_endo * inner(N, u) * ds(geo.ENDO)

    # full problem
    W_tot = W_iso + W_vol + W_ext + W_bcs

    # tangent problem
    v  = TestFunction(mech_space)
    dw = TrialFunction(mech_space)
    G  = derivative(W_tot, mech_state, v)
    #G += p_endo * inner(cofac(F)*N, split(v)[0]) * ds(geo.ENDO)

    dG = derivative(G, mech_state, dw)

    # solver
    solver = VentricleSolver(G, dG, mech_state.vector(), geo, bcs)

    osol = XDMFFile(mesh.mpi_comm(), "sol_inflate.xdmf")
    osol.parameters["rewrite_function_mesh"] = False
    osol.parameters["flush_output"] = True

    import numpy as np
    for pp in np.arange(0.0, 1.0, 0.02) :
        p_endo.assign(pp)
        solver.solve()
        osol << (mech_state.split()[0], pp)
